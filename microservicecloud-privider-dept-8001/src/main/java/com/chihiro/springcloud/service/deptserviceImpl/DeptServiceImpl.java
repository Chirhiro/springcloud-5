package com.chihiro.springcloud.service.deptserviceImpl;

import com.chihiro.microservice.entity.Dept;
import com.chihiro.springcloud.dao.DeptDao;
import com.chihiro.springcloud.service.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DeptServiceImpl implements DeptService {

    @Autowired
    private DeptDao deptDao;


    @Override
    public int insertDept(Dept dept) {
        return deptDao.addDept(dept);
    }


    @Override
    public Dept find(int  deptno) {

        return deptDao.findById(deptno);
    }

    @Override
    public List<Dept> list() {
        return deptDao.findAll();
    }
}
