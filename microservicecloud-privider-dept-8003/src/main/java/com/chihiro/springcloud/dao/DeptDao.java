package com.chihiro.springcloud.dao;


import com.chihiro.microservice.entity.Dept;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface DeptDao {

    /**
     * 增加部门操作
     * @param dept
     * @return
     */
    public int addDept(Dept dept);

    /**
     * 查询部门
     * @param deptno
     * @return
     */
    public Dept findById(int deptno);

    /**
     * 查询部门
     * @param
     * @return
     */
    public List<Dept> findAll();


}
