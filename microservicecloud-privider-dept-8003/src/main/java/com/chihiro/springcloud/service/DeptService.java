package com.chihiro.springcloud.service;

import com.chihiro.microservice.entity.Dept;

import java.util.List;

public interface DeptService {


    /**
     * 增加部门操作
     * @param dept
     * @return
     */
    public int insertDept(Dept dept);



    /**
     * 查询部门
     * @param  deptno
     * @return
     */
    public Dept find(int deptno);


    /**
     * 查询所有部门
     * @param
     * @return
     */
    public List<Dept> list();
}
