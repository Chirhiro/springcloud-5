package com.chihiro.microservice.controller;

import com.chihiro.microservice.entity.Dept;
import com.chihiro.microservice.service.DeptClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Controller
public class DeptController_consumer {

    @Autowired
    private DeptClientService deptClientService;

    @RequestMapping(value = "/consumer/dept/list")
    @ResponseBody
    public List<Dept> list()
    {
        return deptClientService.list();
    }


    @RequestMapping(value = "/consumer/dept/{deptno}")
    @ResponseBody
    public Dept list(@PathVariable("deptno") int deptno)
    {
        return deptClientService.list(deptno);
    }
}
